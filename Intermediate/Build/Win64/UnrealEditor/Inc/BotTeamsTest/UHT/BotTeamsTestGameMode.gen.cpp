// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "BotTeamsTest/BotTeamsTestGameMode.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBotTeamsTestGameMode() {}
// Cross Module References
	BOTTEAMSTEST_API UClass* Z_Construct_UClass_ABotTeamsTestGameMode();
	BOTTEAMSTEST_API UClass* Z_Construct_UClass_ABotTeamsTestGameMode_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_BotTeamsTest();
// End Cross Module References
	void ABotTeamsTestGameMode::StaticRegisterNativesABotTeamsTestGameMode()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(ABotTeamsTestGameMode);
	UClass* Z_Construct_UClass_ABotTeamsTestGameMode_NoRegister()
	{
		return ABotTeamsTestGameMode::StaticClass();
	}
	struct Z_Construct_UClass_ABotTeamsTestGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABotTeamsTestGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_BotTeamsTest,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABotTeamsTestGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering HLOD WorldPartition DataLayers Transformation" },
		{ "IncludePath", "BotTeamsTestGameMode.h" },
		{ "ModuleRelativePath", "BotTeamsTestGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABotTeamsTestGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABotTeamsTestGameMode>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_ABotTeamsTestGameMode_Statics::ClassParams = {
		&ABotTeamsTestGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_ABotTeamsTestGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABotTeamsTestGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABotTeamsTestGameMode()
	{
		if (!Z_Registration_Info_UClass_ABotTeamsTestGameMode.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_ABotTeamsTestGameMode.OuterSingleton, Z_Construct_UClass_ABotTeamsTestGameMode_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_ABotTeamsTestGameMode.OuterSingleton;
	}
	template<> BOTTEAMSTEST_API UClass* StaticClass<ABotTeamsTestGameMode>()
	{
		return ABotTeamsTestGameMode::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABotTeamsTestGameMode);
	ABotTeamsTestGameMode::~ABotTeamsTestGameMode() {}
	struct Z_CompiledInDeferFile_FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestGameMode_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestGameMode_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_ABotTeamsTestGameMode, ABotTeamsTestGameMode::StaticClass, TEXT("ABotTeamsTestGameMode"), &Z_Registration_Info_UClass_ABotTeamsTestGameMode, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(ABotTeamsTestGameMode), 1811691713U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestGameMode_h_753979188(TEXT("/Script/BotTeamsTest"),
		Z_CompiledInDeferFile_FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestGameMode_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestGameMode_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
