// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "BotTeamsTestProjectile.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class UPrimitiveComponent;
struct FHitResult;
#ifdef BOTTEAMSTEST_BotTeamsTestProjectile_generated_h
#error "BotTeamsTestProjectile.generated.h already included, missing '#pragma once' in BotTeamsTestProjectile.h"
#endif
#define BOTTEAMSTEST_BotTeamsTestProjectile_generated_h

#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestProjectile_h_13_SPARSE_DATA
#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestProjectile_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit);


#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestProjectile_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit);


#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestProjectile_h_13_ACCESSORS
#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestProjectile_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABotTeamsTestProjectile(); \
	friend struct Z_Construct_UClass_ABotTeamsTestProjectile_Statics; \
public: \
	DECLARE_CLASS(ABotTeamsTestProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BotTeamsTest"), NO_API) \
	DECLARE_SERIALIZER(ABotTeamsTestProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestProjectile_h_13_INCLASS \
private: \
	static void StaticRegisterNativesABotTeamsTestProjectile(); \
	friend struct Z_Construct_UClass_ABotTeamsTestProjectile_Statics; \
public: \
	DECLARE_CLASS(ABotTeamsTestProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BotTeamsTest"), NO_API) \
	DECLARE_SERIALIZER(ABotTeamsTestProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestProjectile_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABotTeamsTestProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABotTeamsTestProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABotTeamsTestProjectile); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABotTeamsTestProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABotTeamsTestProjectile(ABotTeamsTestProjectile&&); \
	NO_API ABotTeamsTestProjectile(const ABotTeamsTestProjectile&); \
public: \
	NO_API virtual ~ABotTeamsTestProjectile();


#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestProjectile_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABotTeamsTestProjectile(ABotTeamsTestProjectile&&); \
	NO_API ABotTeamsTestProjectile(const ABotTeamsTestProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABotTeamsTestProjectile); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABotTeamsTestProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABotTeamsTestProjectile) \
	NO_API virtual ~ABotTeamsTestProjectile();


#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestProjectile_h_10_PROLOG
#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestProjectile_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestProjectile_h_13_SPARSE_DATA \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestProjectile_h_13_RPC_WRAPPERS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestProjectile_h_13_ACCESSORS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestProjectile_h_13_INCLASS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestProjectile_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestProjectile_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestProjectile_h_13_SPARSE_DATA \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestProjectile_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestProjectile_h_13_ACCESSORS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestProjectile_h_13_INCLASS_NO_PURE_DECLS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestProjectile_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BOTTEAMSTEST_API UClass* StaticClass<class ABotTeamsTestProjectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
