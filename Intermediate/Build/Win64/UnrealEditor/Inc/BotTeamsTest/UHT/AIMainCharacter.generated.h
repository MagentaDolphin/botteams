// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "AIMainCharacter.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BOTTEAMSTEST_AIMainCharacter_generated_h
#error "AIMainCharacter.generated.h already included, missing '#pragma once' in AIMainCharacter.h"
#endif
#define BOTTEAMSTEST_AIMainCharacter_generated_h

#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_AIMainCharacter_h_12_SPARSE_DATA
#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_AIMainCharacter_h_12_RPC_WRAPPERS
#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_AIMainCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_AIMainCharacter_h_12_ACCESSORS
#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_AIMainCharacter_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAIMainCharacter(); \
	friend struct Z_Construct_UClass_AAIMainCharacter_Statics; \
public: \
	DECLARE_CLASS(AAIMainCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BotTeamsTest"), NO_API) \
	DECLARE_SERIALIZER(AAIMainCharacter)


#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_AIMainCharacter_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAAIMainCharacter(); \
	friend struct Z_Construct_UClass_AAIMainCharacter_Statics; \
public: \
	DECLARE_CLASS(AAIMainCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BotTeamsTest"), NO_API) \
	DECLARE_SERIALIZER(AAIMainCharacter)


#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_AIMainCharacter_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAIMainCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAIMainCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAIMainCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAIMainCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAIMainCharacter(AAIMainCharacter&&); \
	NO_API AAIMainCharacter(const AAIMainCharacter&); \
public: \
	NO_API virtual ~AAIMainCharacter();


#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_AIMainCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAIMainCharacter(AAIMainCharacter&&); \
	NO_API AAIMainCharacter(const AAIMainCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAIMainCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAIMainCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAIMainCharacter) \
	NO_API virtual ~AAIMainCharacter();


#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_AIMainCharacter_h_9_PROLOG
#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_AIMainCharacter_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_AIMainCharacter_h_12_SPARSE_DATA \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_AIMainCharacter_h_12_RPC_WRAPPERS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_AIMainCharacter_h_12_ACCESSORS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_AIMainCharacter_h_12_INCLASS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_AIMainCharacter_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_AIMainCharacter_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_AIMainCharacter_h_12_SPARSE_DATA \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_AIMainCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_AIMainCharacter_h_12_ACCESSORS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_AIMainCharacter_h_12_INCLASS_NO_PURE_DECLS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_AIMainCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BOTTEAMSTEST_API UClass* StaticClass<class AAIMainCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_AIMainCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
