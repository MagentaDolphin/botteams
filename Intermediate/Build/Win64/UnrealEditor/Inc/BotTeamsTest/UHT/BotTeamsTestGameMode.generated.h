// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "BotTeamsTestGameMode.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BOTTEAMSTEST_BotTeamsTestGameMode_generated_h
#error "BotTeamsTestGameMode.generated.h already included, missing '#pragma once' in BotTeamsTestGameMode.h"
#endif
#define BOTTEAMSTEST_BotTeamsTestGameMode_generated_h

#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestGameMode_h_12_SPARSE_DATA
#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestGameMode_h_12_RPC_WRAPPERS
#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestGameMode_h_12_ACCESSORS
#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABotTeamsTestGameMode(); \
	friend struct Z_Construct_UClass_ABotTeamsTestGameMode_Statics; \
public: \
	DECLARE_CLASS(ABotTeamsTestGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/BotTeamsTest"), BOTTEAMSTEST_API) \
	DECLARE_SERIALIZER(ABotTeamsTestGameMode)


#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesABotTeamsTestGameMode(); \
	friend struct Z_Construct_UClass_ABotTeamsTestGameMode_Statics; \
public: \
	DECLARE_CLASS(ABotTeamsTestGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/BotTeamsTest"), BOTTEAMSTEST_API) \
	DECLARE_SERIALIZER(ABotTeamsTestGameMode)


#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	BOTTEAMSTEST_API ABotTeamsTestGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABotTeamsTestGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(BOTTEAMSTEST_API, ABotTeamsTestGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABotTeamsTestGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	BOTTEAMSTEST_API ABotTeamsTestGameMode(ABotTeamsTestGameMode&&); \
	BOTTEAMSTEST_API ABotTeamsTestGameMode(const ABotTeamsTestGameMode&); \
public: \
	BOTTEAMSTEST_API virtual ~ABotTeamsTestGameMode();


#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	BOTTEAMSTEST_API ABotTeamsTestGameMode(ABotTeamsTestGameMode&&); \
	BOTTEAMSTEST_API ABotTeamsTestGameMode(const ABotTeamsTestGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(BOTTEAMSTEST_API, ABotTeamsTestGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABotTeamsTestGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABotTeamsTestGameMode) \
	BOTTEAMSTEST_API virtual ~ABotTeamsTestGameMode();


#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestGameMode_h_9_PROLOG
#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestGameMode_h_12_SPARSE_DATA \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestGameMode_h_12_RPC_WRAPPERS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestGameMode_h_12_ACCESSORS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestGameMode_h_12_INCLASS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestGameMode_h_12_SPARSE_DATA \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestGameMode_h_12_ACCESSORS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestGameMode_h_12_INCLASS_NO_PURE_DECLS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BOTTEAMSTEST_API UClass* StaticClass<class ABotTeamsTestGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
