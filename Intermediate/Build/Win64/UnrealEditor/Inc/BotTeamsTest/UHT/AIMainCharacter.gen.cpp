// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "BotTeamsTest/AIMainCharacter.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAIMainCharacter() {}
// Cross Module References
	BOTTEAMSTEST_API UClass* Z_Construct_UClass_AAIMainCharacter();
	BOTTEAMSTEST_API UClass* Z_Construct_UClass_AAIMainCharacter_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_BotTeamsTest();
// End Cross Module References
	void AAIMainCharacter::StaticRegisterNativesAAIMainCharacter()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(AAIMainCharacter);
	UClass* Z_Construct_UClass_AAIMainCharacter_NoRegister()
	{
		return AAIMainCharacter::StaticClass();
	}
	struct Z_Construct_UClass_AAIMainCharacter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AAIMainCharacter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_BotTeamsTest,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAIMainCharacter_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "AIMainCharacter.h" },
		{ "ModuleRelativePath", "AIMainCharacter.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AAIMainCharacter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AAIMainCharacter>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_AAIMainCharacter_Statics::ClassParams = {
		&AAIMainCharacter::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AAIMainCharacter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AAIMainCharacter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AAIMainCharacter()
	{
		if (!Z_Registration_Info_UClass_AAIMainCharacter.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_AAIMainCharacter.OuterSingleton, Z_Construct_UClass_AAIMainCharacter_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_AAIMainCharacter.OuterSingleton;
	}
	template<> BOTTEAMSTEST_API UClass* StaticClass<AAIMainCharacter>()
	{
		return AAIMainCharacter::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(AAIMainCharacter);
	AAIMainCharacter::~AAIMainCharacter() {}
	struct Z_CompiledInDeferFile_FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_AIMainCharacter_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_AIMainCharacter_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_AAIMainCharacter, AAIMainCharacter::StaticClass, TEXT("AAIMainCharacter"), &Z_Registration_Info_UClass_AAIMainCharacter, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(AAIMainCharacter), 412091317U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_AIMainCharacter_h_3792917637(TEXT("/Script/BotTeamsTest"),
		Z_CompiledInDeferFile_FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_AIMainCharacter_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_AIMainCharacter_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
