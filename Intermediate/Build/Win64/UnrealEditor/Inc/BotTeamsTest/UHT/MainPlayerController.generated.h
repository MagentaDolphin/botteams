// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "MainPlayerController.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BOTTEAMSTEST_MainPlayerController_generated_h
#error "MainPlayerController.generated.h already included, missing '#pragma once' in MainPlayerController.h"
#endif
#define BOTTEAMSTEST_MainPlayerController_generated_h

#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_MainPlayerController_h_13_SPARSE_DATA
#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_MainPlayerController_h_13_RPC_WRAPPERS
#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_MainPlayerController_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_MainPlayerController_h_13_ACCESSORS
#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_MainPlayerController_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMainPlayerController(); \
	friend struct Z_Construct_UClass_AMainPlayerController_Statics; \
public: \
	DECLARE_CLASS(AMainPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BotTeamsTest"), NO_API) \
	DECLARE_SERIALIZER(AMainPlayerController) \
	virtual UObject* _getUObject() const override { return const_cast<AMainPlayerController*>(this); }


#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_MainPlayerController_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAMainPlayerController(); \
	friend struct Z_Construct_UClass_AMainPlayerController_Statics; \
public: \
	DECLARE_CLASS(AMainPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BotTeamsTest"), NO_API) \
	DECLARE_SERIALIZER(AMainPlayerController) \
	virtual UObject* _getUObject() const override { return const_cast<AMainPlayerController*>(this); }


#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_MainPlayerController_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMainPlayerController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMainPlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMainPlayerController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMainPlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMainPlayerController(AMainPlayerController&&); \
	NO_API AMainPlayerController(const AMainPlayerController&); \
public: \
	NO_API virtual ~AMainPlayerController();


#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_MainPlayerController_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMainPlayerController(AMainPlayerController&&); \
	NO_API AMainPlayerController(const AMainPlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMainPlayerController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMainPlayerController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMainPlayerController) \
	NO_API virtual ~AMainPlayerController();


#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_MainPlayerController_h_10_PROLOG
#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_MainPlayerController_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_MainPlayerController_h_13_SPARSE_DATA \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_MainPlayerController_h_13_RPC_WRAPPERS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_MainPlayerController_h_13_ACCESSORS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_MainPlayerController_h_13_INCLASS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_MainPlayerController_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_MainPlayerController_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_MainPlayerController_h_13_SPARSE_DATA \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_MainPlayerController_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_MainPlayerController_h_13_ACCESSORS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_MainPlayerController_h_13_INCLASS_NO_PURE_DECLS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_MainPlayerController_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BOTTEAMSTEST_API UClass* StaticClass<class AMainPlayerController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_MainPlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
