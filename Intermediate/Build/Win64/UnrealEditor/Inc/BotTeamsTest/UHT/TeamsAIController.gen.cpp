// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "BotTeamsTest/TeamsAIController.h"
#include "AIModule/Classes/GenericTeamAgentInterface.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTeamsAIController() {}
// Cross Module References
	AIMODULE_API UClass* Z_Construct_UClass_AAIController();
	AIMODULE_API UScriptStruct* Z_Construct_UScriptStruct_FGenericTeamId();
	BOTTEAMSTEST_API UClass* Z_Construct_UClass_ATeamsAIController();
	BOTTEAMSTEST_API UClass* Z_Construct_UClass_ATeamsAIController_NoRegister();
	UPackage* Z_Construct_UPackage__Script_BotTeamsTest();
// End Cross Module References
	void ATeamsAIController::StaticRegisterNativesATeamsAIController()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(ATeamsAIController);
	UClass* Z_Construct_UClass_ATeamsAIController_NoRegister()
	{
		return ATeamsAIController::StaticClass();
	}
	struct Z_Construct_UClass_ATeamsAIController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_TeamID_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_TeamID;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATeamsAIController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AAIController,
		(UObject* (*)())Z_Construct_UPackage__Script_BotTeamsTest,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATeamsAIController_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Collision Rendering Transformation" },
		{ "IncludePath", "TeamsAIController.h" },
		{ "ModuleRelativePath", "TeamsAIController.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATeamsAIController_Statics::NewProp_TeamID_MetaData[] = {
		{ "Category", "Teams" },
		{ "ModuleRelativePath", "TeamsAIController.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_ATeamsAIController_Statics::NewProp_TeamID = { "TeamID", nullptr, (EPropertyFlags)0x0020080000000005, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(ATeamsAIController, TeamID), Z_Construct_UScriptStruct_FGenericTeamId, METADATA_PARAMS(Z_Construct_UClass_ATeamsAIController_Statics::NewProp_TeamID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATeamsAIController_Statics::NewProp_TeamID_MetaData)) }; // 4223950188
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ATeamsAIController_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATeamsAIController_Statics::NewProp_TeamID,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATeamsAIController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATeamsAIController>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_ATeamsAIController_Statics::ClassParams = {
		&ATeamsAIController::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ATeamsAIController_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ATeamsAIController_Statics::PropPointers),
		0,
		0x009002A4u,
		METADATA_PARAMS(Z_Construct_UClass_ATeamsAIController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATeamsAIController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATeamsAIController()
	{
		if (!Z_Registration_Info_UClass_ATeamsAIController.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_ATeamsAIController.OuterSingleton, Z_Construct_UClass_ATeamsAIController_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_ATeamsAIController.OuterSingleton;
	}
	template<> BOTTEAMSTEST_API UClass* StaticClass<ATeamsAIController>()
	{
		return ATeamsAIController::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATeamsAIController);
	ATeamsAIController::~ATeamsAIController() {}
	struct Z_CompiledInDeferFile_FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_TeamsAIController_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_TeamsAIController_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_ATeamsAIController, ATeamsAIController::StaticClass, TEXT("ATeamsAIController"), &Z_Registration_Info_UClass_ATeamsAIController, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(ATeamsAIController), 2232677863U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_TeamsAIController_h_3864601706(TEXT("/Script/BotTeamsTest"),
		Z_CompiledInDeferFile_FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_TeamsAIController_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_TeamsAIController_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
