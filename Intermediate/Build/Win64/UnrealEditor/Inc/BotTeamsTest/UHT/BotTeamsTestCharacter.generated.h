// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "BotTeamsTestCharacter.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BOTTEAMSTEST_BotTeamsTestCharacter_generated_h
#error "BotTeamsTestCharacter.generated.h already included, missing '#pragma once' in BotTeamsTestCharacter.h"
#endif
#define BOTTEAMSTEST_BotTeamsTestCharacter_generated_h

#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestCharacter_h_20_SPARSE_DATA
#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestCharacter_h_20_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetHasRifle); \
	DECLARE_FUNCTION(execSetHasRifle);


#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestCharacter_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetHasRifle); \
	DECLARE_FUNCTION(execSetHasRifle);


#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestCharacter_h_20_ACCESSORS
#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestCharacter_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABotTeamsTestCharacter(); \
	friend struct Z_Construct_UClass_ABotTeamsTestCharacter_Statics; \
public: \
	DECLARE_CLASS(ABotTeamsTestCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BotTeamsTest"), NO_API) \
	DECLARE_SERIALIZER(ABotTeamsTestCharacter)


#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestCharacter_h_20_INCLASS \
private: \
	static void StaticRegisterNativesABotTeamsTestCharacter(); \
	friend struct Z_Construct_UClass_ABotTeamsTestCharacter_Statics; \
public: \
	DECLARE_CLASS(ABotTeamsTestCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BotTeamsTest"), NO_API) \
	DECLARE_SERIALIZER(ABotTeamsTestCharacter)


#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestCharacter_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABotTeamsTestCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABotTeamsTestCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABotTeamsTestCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABotTeamsTestCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABotTeamsTestCharacter(ABotTeamsTestCharacter&&); \
	NO_API ABotTeamsTestCharacter(const ABotTeamsTestCharacter&); \
public: \
	NO_API virtual ~ABotTeamsTestCharacter();


#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestCharacter_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABotTeamsTestCharacter(ABotTeamsTestCharacter&&); \
	NO_API ABotTeamsTestCharacter(const ABotTeamsTestCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABotTeamsTestCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABotTeamsTestCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABotTeamsTestCharacter) \
	NO_API virtual ~ABotTeamsTestCharacter();


#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestCharacter_h_17_PROLOG
#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestCharacter_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestCharacter_h_20_SPARSE_DATA \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestCharacter_h_20_RPC_WRAPPERS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestCharacter_h_20_ACCESSORS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestCharacter_h_20_INCLASS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestCharacter_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestCharacter_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestCharacter_h_20_SPARSE_DATA \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestCharacter_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestCharacter_h_20_ACCESSORS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestCharacter_h_20_INCLASS_NO_PURE_DECLS \
	FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestCharacter_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BOTTEAMSTEST_API UClass* StaticClass<class ABotTeamsTestCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Unreal_Projects_BotTeamsTest_Source_BotTeamsTest_BotTeamsTestCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
