// Copyright Epic Games, Inc. All Rights Reserved.

#include "BotTeamsTestGameMode.h"
#include "BotTeamsTestCharacter.h"
#include "UObject/ConstructorHelpers.h"

ABotTeamsTestGameMode::ABotTeamsTestGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPerson/Blueprints/BP_FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

}
