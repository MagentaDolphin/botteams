

#include "TeamsAIController.h"

ATeamsAIController::ATeamsAIController()
{
	SetGenericTeamId(TeamID);
}

ETeamAttitude::Type ATeamsAIController::GetTeamAttitudeTowards(const AActor& Other) const
{
	if (const APawn* OtherPawn = Cast<APawn>(&Other)) {
     

		if (const IGenericTeamAgentInterface* TeamAgent = Cast<IGenericTeamAgentInterface>(OtherPawn->GetController()))
		{
			FGenericTeamId OtherTeamID = TeamAgent->GetGenericTeamId();
			if (OtherTeamID == this->GetGenericTeamId()) {
				return ETeamAttitude::Neutral;
			}
			else {
				return ETeamAttitude::Hostile;
			}
		}
	}

	return ETeamAttitude::Neutral;
}

FGenericTeamId ATeamsAIController::GetGenericTeamId() const
{
	return TeamID;
}
