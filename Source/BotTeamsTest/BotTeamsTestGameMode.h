// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BotTeamsTestGameMode.generated.h"

UCLASS(minimalapi)
class ABotTeamsTestGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ABotTeamsTestGameMode();
};



