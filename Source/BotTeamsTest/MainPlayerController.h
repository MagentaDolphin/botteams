
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "GenericTeamAgentInterface.h"
#include "MainPlayerController.generated.h"


UCLASS()
class BOTTEAMSTEST_API AMainPlayerController : public APlayerController, public IGenericTeamAgentInterface
{
	GENERATED_BODY()
public:
	AMainPlayerController();
	virtual ETeamAttitude::Type GetTeamAttitudeTowards(const AActor& Other) const override;

protected:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "TeamID")
	FGenericTeamId TeamID = 1;
	FGenericTeamId GetGenericTeamId() const override;
};
