
#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "GenericTeamAgentInterface.h"
#include "TeamsAIController.generated.h"

/**
 * 
 */
UCLASS()
class BOTTEAMSTEST_API ATeamsAIController : public AAIController
{
	GENERATED_BODY()
	ATeamsAIController();
public:
	virtual ETeamAttitude::Type GetTeamAttitudeTowards(const AActor& Other) const override;
	
protected:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Teams")
	FGenericTeamId TeamID = 10;
	FGenericTeamId GetGenericTeamId() const override;
};
