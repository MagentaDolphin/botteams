// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthActorComponent.h"


// Sets default values for this component's properties
UHealthActorComponent::UHealthActorComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	MaxHealth = 100.f;
	Health = MaxHealth;
}


void UHealthActorComponent::BeginPlay()
{
	Super::BeginPlay();

	if(AActor* Owner = GetOwner())
	{
		Owner->OnTakeAnyDamage.AddDynamic(this, &UHealthActorComponent::TakeDamage);
	}
	
	
}

void UHealthActorComponent::TakeDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if(Damage <= 0) return;

	Health = FMath::Clamp(Health - Damage, 0.f, MaxHealth);
}


